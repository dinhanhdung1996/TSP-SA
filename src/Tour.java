import java.util.ArrayList;
import java.util.Collections;

public class Tour {

    private ArrayList<City> tour = new ArrayList<>();
    private double distance = 0;

    public Tour(){
        for(int i = 0; i < TourManager.numberOfCity(); i++){
            tour.add(null);
        }
    }

    public Tour(Tour tour){
        this.setTour(tour.getTour());
    }

    public ArrayList<City> getTour() {
        return tour;
    }

    public void setTour(ArrayList<City> tour) {
        this.tour = (ArrayList<City>)tour.clone();
    }

    public void generateTour(){
        setTour(new TourManager().getTour());
        Collections.shuffle(tour);
    }

    public double getDistance(){
        if(distance == 0){
            double tourDistance = 0;
            for(int i = 0; i< tour.size(); i++){
                if(i + 1 < tour.size()){
                    tourDistance += tour.get(i).distanceTo(tour.get(i+1));
                }
                else{
                    tourDistance += tour.get(i).distanceTo(tour.get(0));
                }
            }
            distance = tourDistance;
        }
        return distance;
    }

    public void setCity(int index, City city){
        tour.set(index, city);
    }

    public void swapCity(int index1, int index2){
        if(index1 >= tour.size() || index2 >= tour.size()){
            return;
        }
        City city1 = tour.get(index1);
        City city2 = tour.get(index2);
        setCity(index1, city2);
        setCity(index2, city1);
    }

    public int size(){
        return tour.size();
    }

    public String toString(){
        return tour.toString();
    }

}
