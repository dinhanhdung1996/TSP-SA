public class City {
    private double x;
    private double y;
    private int id;

    public City(int id, double x, double y){
        this.x = x;
        this.y = y;
        this.id = id;
    }

    public City(double x, double y){
        this.x = x;
        this.y = y;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }

    public void setX(double x){
        this.x = x;
    }

    public void setY(double y){
        this.y = y;
    }


    public City(){

    }

    public double distanceTo(City city){
        return Math.sqrt(Math.pow(x - city.getX(), 2) + Math.pow(y - city.getY(), 2));
    }


    public String toString(){
        //return "(" + getX() + "," + getY()+ ")";
        return "" + id;
    }


}
