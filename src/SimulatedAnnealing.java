import java.util.Random;

public class SimulatedAnnealing {
    private Tour tour;
    private Tour best;
    private double temperature = 100000;
    private static double coolingRate = 0.00003;
    public SimulatedAnnealing(Tour tour){
        this.tour = tour;
        best = new Tour(tour);
    }

    public double acceptanceSolution(double currentEnergy, double newEnergy, double temperature){
        if(newEnergy < currentEnergy){
            return 1;
        }
        else{
            return  Math.exp((newEnergy - currentEnergy)/ temperature);
        }
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public Tour SA(){
        best = new Tour(tour);
        if(tour == null)return null;
        while(temperature > 1){
       // while(true){
            System.out.println("Current : " + tour.getDistance() + " Best: " + best.getDistance());
            int cityPos1 = new Random().nextInt(tour.size() + 1);
            int cityPos2 = new Random().nextInt(tour.size() + 1);
            Tour temp = new Tour();
            temp.setTour(tour.getTour());
            temp.swapCity(cityPos1, cityPos2);
            double currentEnergy = tour.getDistance();
            double newEnergy = temp.getDistance();
            if(acceptanceSolution(currentEnergy, newEnergy, temperature)> new Random().nextDouble()){
                tour = new Tour();
                tour.setTour(temp.getTour());
            }
            if(best.getDistance() > tour.getDistance()){
                best = new Tour(tour);
            }
            temperature *= (1 - coolingRate);
        }
        return best;
    }

    public Tour getBest() {
        return best;
    }
}
