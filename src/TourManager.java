import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class TourManager {

    private static ArrayList<City> tour = new ArrayList<>();

    public void setData(String fileName){
        tour = new ArrayList<>();
        try {

            Scanner scanner = new Scanner(new File(fileName));
            while(scanner.hasNext()) {
                City city = new City();
                int id = scanner.nextInt();
                city.setId(id);
                double x = scanner.nextDouble();
                city.setX(x);
                double y = scanner.nextDouble();
                city.setY(y);
                System.out.println("" + id + " " + x + " " + y);
                tour.add(city);
            }
        }catch (Exception e){
            System.out.println(e);
        }
    }

    public static void addCity(City city){
        tour.add(city);
    }


    public ArrayList<City> getTour() {
        return tour;
    }

    public City getCity(int index){
        if(index >= tour.size())return null;
        return tour.get(index);
    }

    public void setCity(int index, City city){
        if(index >= tour.size())return;
        tour.set(index, city);
    }

    public static int numberOfCity(){
        return tour.size();
    }

}
